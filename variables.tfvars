//required variables

database_name              = "mydatabase"
database_instance          = "dbinstance"
instance_database_version  = "POSTGRES_11"
project                    = "omega-winter-329511"
user_instance              = "userinstance"
user_name                  = "dbuser"

//optional variables

database_charset = "UTF8"
database_collation = "en_US.UTF8"
instance_name = "myinstance"
instance_region = "us-central1"
instance_master_instance_name = "masterinstance"
user_password      = "userpswd"
user_type = "BUILT_IN"
user_deletion_policy ="FALSE"
instance_settings_tier ="db-f1-micro"
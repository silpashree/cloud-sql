//required variables

variable "database_name" {
  description = "The name of the database in the Cloud SQL instance."
  type        = string
}

variable "database_instance" {
  description = "The name of the Cloud SQL instance."
  type        = string
}

variable "instance_database_version" {
  description = "The MySQL, PostgreSQL or SQL Server version to use."
  type        = string
}

variable "user_instance" {
  description = "The name of the Cloud SQL instance."
  type        = string
}

variable "user_name" {
  description = "The name of the user."
  type        = string
}

variable "project" {
  description = "The ID of the project in which the resource belongs."
  type        = string
}

//optional variables
variable "database_charset" {
  description = "The charset value. "
  type        = string
  default     = "UTF8"
}

variable "database_collation" {
  description = "The collation value."
  type        = string
  default     = "en_US.UTF8"
}

variable "instance_name" {
  description = "The name of the instance."
  type        = string
  default     = "my_instance"
}

variable "instance_region" {
  description = "The region the instance will sit in."
  type        = string
  default     = "us-central1"
}

variable "instance_master_instance_name" {
  description = "The name of the existing instance that will act as the master in the replication setup. "
  type        = string
  default     = ""
}

variable "instance_settings_tier" {
  description = "The machine type to use."
  type        = string
  default     = ""
}

variable "user_password" {
  description = "The password for the user."
  type        = string
  default     = ""
}

variable "user_type" {
  description = "The user type."
  type        = string
  default     = "BUILT_IN"
}

variable "user_deletion_policy" {
  description = "The deletion policy for the user."
  type        = string
  default     = "FALSE"
}

variable "user_host" {
  description  = "The host the user can connect from."
  type         =  string
  default      = ""
}
//sql_databse_instance
resource "google_sql_database_instance" "sql_instance" {
  name   = var.instance_name
  region = var.instance_region
  settings {
    tier = var.instance_settings_tier
  }
  database_version     = var.instance_database_version
  project              = var.project
}

